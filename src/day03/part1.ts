import { parseInput } from '../utils/index.js';

interface IPosition {
  row: number;
  col: number;
  len: number;
}

export const getNumLength = (num: number): number => {
  return `${num}`.length;
};

const specialChars = ['-', '@', '*', '/', '&', '#', '%', '+', '=', '$'];

export const isSpecialCharAdjacent = ({ row, col, len }: IPosition, data: string[]): boolean => {
  let isAdjacent = false;

  for (let rowIdx = row - 1; rowIdx <= row + 1; rowIdx++) {
    for (let colIdx = col - 1; colIdx <= col + len; colIdx++) {
      const char = (data[rowIdx] ?? [])[colIdx] ?? '';
      if (specialChars.includes(char)) {
        isAdjacent = true;
        break;
      }
    }
  }

  return isAdjacent;
};

const numRegex = /\d*/g;
interface IParseNumbersResponse {
  col: number;
  num: number;
}

export const parseNumbers = (line: string): IParseNumbersResponse[] => {
  const numStrs = line.match(numRegex)?.filter(Boolean) ?? [];
  const response = [];
  let curCol = 0;

  for (let idx = 0; idx < numStrs.length; idx++) {
    const numStr = numStrs[idx];
    const numLength = numStr.length;
    const col = line.slice(curCol).indexOf(numStr) + curCol;
    response.push({ col, num: parseInt(numStr, 10) });
    curCol = (col + numLength);
  }

  return response;
};

export const parseMultipleLines = (lines: string[]): number => {
  return lines.reduce((acc, line, row) => {
    const parsedNums = parseNumbers(line);

    for (let idx = 0; idx < parsedNums.length; idx++) {
      const { num, col } = parsedNums[idx];
      const len = `${num}`.length;
      if (isSpecialCharAdjacent({ row, col, len }, lines)) {
        acc += num;
      }
    }

    return acc;
  }, 0);
};

export const part1 = (rawInput: string): number => {
  const input = parseInput(rawInput);

  return parseMultipleLines(input);
};
