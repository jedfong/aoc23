import { parseInput } from '../utils/index.js';

interface IPosition {
  row: number;
  col: number;
  len: number;
}

const numRegex = /\d*/g;

interface IParseNumbersResponse {
  col: number;
  num: number;
}

export const parseNumbers = (line: string): IParseNumbersResponse[] => {
  const numStrs = line.match(numRegex)?.filter(Boolean) ?? [];
  const response = [];
  let curCol = 0;

  for (let idx = 0; idx < numStrs.length; idx++) {
    const numStr = numStrs[idx];
    const numLength = numStr.length;
    const col = line.slice(curCol).indexOf(numStr) + curCol;
    response.push({ col, num: parseInt(numStr, 10) });
    curCol = (col + numLength);
  }

  return response;
};

interface IPositionWithNum extends IPosition {
  num: number;
}

export const getNumbersTouchingChar = ({ row, col }: IPosition, data: string[]): IPositionWithNum[] => {
  const nums: IPositionWithNum[] = [];

  for (let rowIdx = row - 1; rowIdx <= row + 1; rowIdx++) {
    const rowNums = parseNumbers(data[rowIdx]);
    for (let numIdx = 0; numIdx < rowNums.length; numIdx++) {
      const rowNum = rowNums[numIdx];
      const numLen = `${rowNum.num}`.length;
      if (col >= rowNum.col - 1 && col <= rowNum.col + numLen) {
        nums.push({ row: rowIdx, col: rowNum.col, len: numLen, num: rowNum.num });
      }
    }
  }

  return nums;
};

const specialChars = ['-', '@', '*', '/', '&', '#', '%', '+', '=', '$'];

export const getGearRatios = (row: number, lines: string[]): number[] => {
  const gearRatios: number[] = [];

  for (let col = 0; col < lines[row].length; col++) {
    const char = lines[row][col];
    if (specialChars.includes(char)) {
      const numbersTouchingChar = getNumbersTouchingChar({ row, col, len: 1 }, lines);
      if (numbersTouchingChar.length === 2) {
        gearRatios.push(numbersTouchingChar[0].num * numbersTouchingChar[1].num);
      }
    }
  }

  return gearRatios;
};

export const parseMultipleLines = (lines: string[]): number => {
  return lines.reduce((acc, line, row) => {
    const gearRatios = getGearRatios(row, lines);

    for (let idx = 0; idx < gearRatios.length; idx++) {
      acc += gearRatios[idx];
    }

    return acc;
  }, 0);
};

export const part2 = (rawInput: string): number => {
  const input = parseInput(rawInput);

  return parseMultipleLines(input);
};
