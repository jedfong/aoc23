import { describe, expect, test } from 'vitest';
import { isSpecialCharAdjacent, parseNumbers, part1 } from './part1';

test('parseNumbers returns true when adjacent', () => {
  const parsed = parseNumbers('.42.6');
  expect(parsed[0]).toEqual({ num: 42, col: 1 });
  expect(parsed[1]).toEqual({ num: 6, col: 4 });
});

test('isSpecialCharAdjacent returns true when adjacent', () => {
  const data = [
    '....',
    '.23.',
    '...$',
  ];

  expect(isSpecialCharAdjacent({ row: 1, col: 1, len: 2 }, data)).toBe(true);
});

describe('part 1', () => {
  test('from example', () => {
    const input = `
      467..114..
      ...*......
      ..35..633.
      ......#...
      617*......
      .....+.58.
      ..592.....
      ......755.
      ...$.*....
      .664.598..
    `;
    expect(part1(input)).toEqual(4361);
  });
});
