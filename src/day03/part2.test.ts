import { describe, expect, test } from 'vitest';
import { part2 } from './part2';

describe('part 2', () => {
  test('from example', () => {
    const input = `
      467..114..
      ...*......
      ..35..633.
      ......#...
      617*......
      .....+.58.
      ..592.....
      ......755.
      ...$.*....
      .664.598..
    `;
    expect(part2(input)).toEqual(467835);
  });
});
