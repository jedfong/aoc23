import { parseInput } from '../utils/index.js';

export const parseSingleLine = (line: string): number => {
  return 0;
};

export const parseMultipleLines = (lines: string[]): number => {
  return lines.reduce((acc, line) => {
    return acc + parseSingleLine(line);
  }, 0);
};

export const part2 = (rawInput: string): number => {
  const input = parseInput(rawInput);

  return parseMultipleLines(input);
};
