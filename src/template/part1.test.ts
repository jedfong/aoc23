import { describe, expect, test } from 'vitest';
import { parseSingleLine, part1 } from './part1';

describe.each([
  // ['6798seven', 68],
])('parseSingleLine(%s)', (input, expected) => {
  test(`returns ${expected}`, () => {
    expect(parseSingleLine(input)).toBe(expected);
  });
});

describe('part 1', () => {
  test('from example', () => {
    const input = `abc`;
    expect(part1(input)).toEqual(0);
  });
});
