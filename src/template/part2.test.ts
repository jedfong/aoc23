import { describe, expect, test } from 'vitest';
import { parseSingleLine, part2 } from './part2';

describe.each([
  // ['6798seven', 68],
])('parseSingleLine(%s)', (input, expected) => {
  test(`returns ${expected}`, () => {
    expect(parseSingleLine(input)).toBe(expected);
  });
});

describe('part 2', () => {
  test('from example', () => {
    const input = `abc`;
    expect(part2(input)).toEqual(0);
  });
});
