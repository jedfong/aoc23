import { intersects, parseInput, parseNumbers } from '../utils/index.js';

export const parseSingleLine = (line: string): {win: number[], mine: number[]} => {
  const nums = line.split(':')[1];
  const [rawWin, rawMine] = nums.split('|');

  return {
    win: parseNumbers(rawWin),
    mine: parseNumbers(rawMine),
  };
};

export const parseMultipleLines = (lines: string[]): number => {
  const initCopies = Array.from('1'.repeat(lines.length)).map(s => parseInt(s, 10));

  const copies = lines.reduce<number[]>((acc, line, idx) => {
    const { win, mine } = parseSingleLine(line);
    const winningNumbers = intersects(win, mine);

    if (winningNumbers.length > 0) {
      for (let winIdx = 0; winIdx < winningNumbers.length; winIdx++) {
        const wonCardIdx = idx + winIdx + 1;
        if (wonCardIdx < lines.length) {
          acc[wonCardIdx] += acc[idx];
        }
      }
    }

    return acc;
  }, initCopies);

  return copies.reduce((acc, n) => acc + n, 0);
};

export const part2 = (rawInput: string): number => {
  const input = parseInput(rawInput);

  return parseMultipleLines(input);
};
