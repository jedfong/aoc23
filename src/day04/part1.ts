import { intersects, parseInput, parseNumbers } from '../utils/index.js';

export const parseSingleLine = (line: string): {win: number[], mine: number[]} => {
  const nums = line.split(':')[1];
  const [rawWin, rawMine] = nums.split('|');

  return {
    win: parseNumbers(rawWin),
    mine: parseNumbers(rawMine),
  };
};

const double = (num: number, times: number): number => {
  return Array.from('x'.repeat(times)).reduce(acc => {
    return acc *= 2;
  }, num);
};

export const parseMultipleLines = (lines: string[]): number => {
  return lines.reduce((acc, line) => {
    const { win, mine } = parseSingleLine(line);
    const winningNumbers = intersects(win, mine);
    if (winningNumbers.length === 0) {
      return acc;
    }

    return acc + double(1, winningNumbers.length - 1);
  }, 0);
};

export const part1 = (rawInput: string): number => {
  const input = parseInput(rawInput);

  return parseMultipleLines(input);
};
