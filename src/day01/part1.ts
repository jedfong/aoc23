import { parseInput } from '../utils/index.js';

export const parseSingleLine = (line: string): number => {
  const [first = 0, last = 0] = line.split('').reduce<[number, number]>((acc, c) => {
    const possibleInt = parseInt(c, 10);

    if (acc[0] === -1 && !isNaN(possibleInt)) {
      acc[0] = possibleInt;
    }

    if (!isNaN(possibleInt)) {
      acc[1] = possibleInt;
    }

    return acc;
  }, [-1, -1]);

  return parseInt(`${first}${last}`, 10);
};

export const parseMultipleLines = (lines: string[]): number => {
  return lines.reduce((acc, line) => {
    return acc + parseSingleLine(line);
  }, 0);
};

export const part1 = (rawInput: string): number | undefined => {
  const input = parseInput(rawInput);

  return parseMultipleLines(input);
};
