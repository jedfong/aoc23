import { describe, expect, test } from 'vitest';
import { parseSingleLine, part1 } from './part1';

describe.each([
  ['6798seven', 68],
  ['six8b32csscsdgjsevenfivedlhzhc', 82],
])('parseSingleLine(%s)', (input, expected) => {
  test(`returns ${expected}`, () => {
    expect(parseSingleLine(input)).toBe(expected);
  });
});

describe('part 1', () => {
  test('from example', () => {
    const input = `
      1abc2
      pqr3stu8vwx
      a1b2c3d4e5f
      treb7uchet
    `;
    expect(part1(input)).toEqual(142);
  });
});
