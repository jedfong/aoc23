import { parseInput } from '../utils/index.js';

const dictionary = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

export const parseSingleLine = (line: string): number => {
  const parsedNumbers: number[] = [];

  for (let idx = 0; idx < line.length; idx++) {
    const parsedNum = parseInt(line[idx], 10);
    if (!isNaN(parsedNum)) {
      parsedNumbers.push(parsedNum);
      // eslint-disable-next-line no-continue
      continue;
    }

    const part = line.substring(idx);
    for (let j = 0; j < dictionary.length; j++) {
      if (part.startsWith(dictionary[j])) {
        parsedNumbers.push(j);
        break;
      }
    }
  }

  return parseInt(`${parsedNumbers[0]}${parsedNumbers[parsedNumbers.length - 1]}`, 10);
};

export const parseMultipleLines = (lines: string[]): number => {
  return lines.reduce((acc, line) => {
    return acc + parseSingleLine(line);
  }, 0);
};

export const part2 = (rawInput: string): number | undefined => {
  const input = parseInput(rawInput);

  return parseMultipleLines(input);
};
