import { describe, expect, test } from 'vitest';
import { parseSingleLine, part2 } from './part2';

describe.each([
  ['two1nine', 29],
  ['eightwothree', 83],
  ['abcone2threexyz', 13],
  ['xtwone3four', 24],
  ['4nineeightseven2', 42],
  ['zoneight234', 14],
  ['7pqrstsixteen', 76],
])('parseSingleLine(%s)', (input, expected) => {
  test(`returns ${expected}`, () => {
    expect(parseSingleLine(input)).toBe(expected);
  });
});

describe('part 2', () => {
  test('from example', () => {
    const input = `
      two1nine
      eightwothree
      abcone2threexyz
      xtwone3four
      4nineeightseven2
      zoneight234
      7pqrstsixteen
    `;
    expect(part2(input)).toEqual(281);
  });
});
