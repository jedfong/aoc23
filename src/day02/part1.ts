import { parseInput } from '../utils/index.js';

interface IHandful {
  r: number;
  g: number;
  b: number;
}

interface IGame {
  id: number;
  r: number;
  g: number;
  b: number;
}

const totalBagCount = {
  r: 12,
  g: 13,
  b: 14,
};

const parseId = (line: string): number => {
  return parseInt(line.split(':')[0].split(' ')[1], 10);
};

export const parseHandful = (handful: string): IHandful => {
  return handful.split(',').reduce((acc, c) => {
    const [countStr, color] = c.trim().split(' ');
    const count = parseInt(countStr, 10) ?? 0;

    if (color === 'red' && count > acc.r) {
      acc.r = count;
    } else if (color === 'green' && count > acc.g) {
      acc.g = count;
    } else if (color === 'blue' && count > acc.b) {
      acc.b = count;
    }

    return acc;
  }, { r: 0, g: 0, b: 0 });
};

const parseColors = (line: string): IHandful => {
  const rawHandfuls = line.split(':')[1].split(';');

  return rawHandfuls.reduce<IHandful>((acc, rawHandful) => {
    const handful = parseHandful(rawHandful);

    if (handful.r > acc.r) {
      acc.r = handful.r;
    }

    if (handful.g > acc.g) {
      acc.g = handful.g;
    }

    if (handful.b > acc.b) {
      acc.b = handful.b;
    }

    return acc;
  }, { r: 0, g: 0, b: 0 });
};

export const parseSingleLine = (line: string): IGame => {
  const id = parseId(line);
  const colors = parseColors(line);
  return {
    id,
    ...colors,
  };
};

const gameIsValid = (game: IGame): boolean => {
  return game.r <= totalBagCount.r
    && game.g <= totalBagCount.g
    && game.b <= totalBagCount.b;
};

export const parseMultipleLines = (lines: string[]): number => {
  return lines.reduce((acc, line) => {
    const game = parseSingleLine(line);
    if (gameIsValid(game)) {
      return acc + game.id;
    }
    return acc;
  }, 0);
};

export const part1 = (rawInput: string): number => {
  const input = parseInput(rawInput);

  return parseMultipleLines(input);
};
