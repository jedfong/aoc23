import { describe, expect, test } from 'vitest';
import { parseHandful, parseSingleLine, part1 } from './part1';

describe.each([
  ['3 blue, 4 red', { r: 4, g: 0, b: 3 }],
  ['1 red, 2 green, 6 blue', { r: 1, g: 2, b: 6 }],
  ['2 green', { r: 0, g: 2, b: 0 }],
])('parseHandful(%s)', (input, expected) => {
  test(`returns ${expected}`, () => {
    expect(parseHandful(input)).toEqual(expected);
  });
});

describe.each([
  ['Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green', { id: 1, r: 4, g: 2, b: 6 }],
  ['Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue', { id: 2, r: 1, g: 3, b: 4 }],
])('parseSingleLine(%s)', (input, expected) => {
  test(`returns ${expected}`, () => {
    expect(parseSingleLine(input)).toEqual(expected);
  });
});

// describe.each([
//   ['Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green', 1],
//   // ['Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue', 2],
//   // ['Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red', 0],
//   // ['Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red', 0],
//   // ['Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green', 5],
// ])('parseSingleLine(%s)', (input, expected) => {
//   test(`returns ${expected}`, () => {
//     expect(parseSingleLine(input)).toBe(expected);
//   });
// });

describe('part 1', () => {
  test('from example', () => {
    const input = `abc`;
    expect(part1(input)).toEqual(0);
  });
});
