import { describe, expect, test } from 'vitest';
import { part2 } from './part2';

describe('part 2', () => {
  test('from example', () => {
    const input = `
      Game 1: 4 blue, 7 red, 5 green; 3 blue, 4 red, 16 green; 3 red, 11 green
      Game 2: 20 blue, 8 red, 1 green; 1 blue, 2 green, 8 red; 9 red, 4 green, 18 blue; 2 green, 7 red, 2 blue; 10 blue, 2 red, 5 green
    `;
    expect(part2(input)).toEqual(1348);
  });
});
