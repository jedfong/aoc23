import { parseInput } from '../utils/index.js';
import { parseSingleLine } from './part1.js';

export const parseMultipleLines = (lines: string[]): number => {
  return lines.reduce((acc, line) => {
    const game = parseSingleLine(line);
    const sum = game.r * game.g * game.b;
    return acc + sum;
  }, 0);
};

export const part2 = (rawInput: string): number => {
  const input = parseInput(rawInput);

  return parseMultipleLines(input);
};
