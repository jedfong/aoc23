export const parseInput = (rawInput: string): string[] => {
  return rawInput.split('\n').map(i => i.trim()).filter(Boolean);
};

export const replaceAll = (str: string, find: string, replace: string): string => {
  return str.replace(new RegExp(find, 'g'), replace);
};

const numRegex = /\d*/g;

export const parseNumbers = (str: string): number[] => {
  return (str.match(numRegex)?.filter(Boolean) ?? []).map(rn => parseInt(rn, 10));
};

export const intersects = <T>(arr1: T[], arr2: T[]): T[] => {
  return arr1.filter(val => arr2.includes(val));
};
