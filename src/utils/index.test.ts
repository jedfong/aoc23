import { describe, expect, test } from 'vitest';
import { parseInput } from './index';

describe('parse input', () => {
  test('should parse input', () => {
    expect(parseInput('foo')).toEqual(['foo']);
  });
});
